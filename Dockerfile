from docker

# Install updates and dependencies
RUN apk update && apk add \
  postgresql \
  postgresql-libs \
  python3 \
  python3-dev \
  build-base \
  postgresql-dev \
  git \
  bash

# Install pip
RUN python3 -m ensurepip

# Add package source
ADD . /app
WORKDIR /app/

# Install python dependencies
RUN python3 -m pip install --upgrade pip \
 && python3 -m pip install -r requirements.dev.txt \
 && python3 -m pip install -e .

# Optional: Run tests. Use like this:
# docker build --network i2b2-stack_i2b2-net . -t i2b2rls
RUN pytest --cov=. || true

# Create volume for config data
VOLUME /data/
WORKDIR /data

cmd bash
