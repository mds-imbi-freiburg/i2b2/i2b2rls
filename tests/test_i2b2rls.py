import os, shutil
import tempfile
from i2b2rls.i2b2fire import i2b2fire, main
from i2b2rls.i2b2fire import main as i2b2firemain
from i2b2rls.utils import *
from i2b2rls.conf import LDAP_DEFAULTS
from i2b2rls import cli
import pytest


TEST_CONNECTION_STRING = "postgresql://i2b2:demouser@localhost:5432/i2b2"
run_dir = os.path.dirname(os.path.abspath(__file__))


@pytest.fixture(scope="module")
def _i2b2fire():
    i = i2b2fire(conf="i2b2rls/config.default", verbose=True)
    yield i
    i.user.delete("cli_test")
    i.user.delete("test")
    i.project.unregister_all()


def test_i2b2fire_init_folder():
    with tempfile.TemporaryDirectory() as tempdir:
        i2b2fire.init(tempdir)
        assert os.path.isfile(tempdir + "/i2b2.yaml")
        assert os.path.isfile(tempdir + "/template-ds.xml")
        assert os.path.isdir(tempdir + "/roles")


def test_project_list_examples(_i2b2fire):
    projects = _i2b2fire.project.list().keys()
    assert "test" not in list(projects)
    assert all(x in list(projects) for x in ["default", "rlsdemo", "mini", "Demo"])


test_user_data = {
    "changeby_char": "i2b2rls",
    "email": None,
    "full_name": "Cli_Test",
    "password": None,
    "project_path": None,
    "status_cd": "A",
    "user_id": "cli_test",
}


def test_user_cli(_i2b2fire):
    # Remove user cli_test if exists or not
    _i2b2fire.user.delete("cli_test")
    # Test user.add is True if it is created, else raises
    assert _i2b2fire.cmd.user.add("cli_test") is None
    with pytest.raises(SystemExit):
        _i2b2fire.cmd.user.add("cli_test")
    print(_i2b2fire.user.get("cli_test"))
    assert _i2b2fire.cmd.user.status("cli_test") is None
    assert _i2b2fire.cmd.user.lock("cli_test") is None
    assert _i2b2fire.cmd.user.is_locked("cli_test") is None
    assert _i2b2fire.cmd.user.unlock("cli_test") is None
    with pytest.raises(SystemExit):
        _i2b2fire.cmd.user.is_locked("cli_test")
    data = _i2b2fire.cmd.user.show("cli_test", fmt=None)
    del data["entry_date"]
    del data["change_date"]
    assert data == test_user_data
    # Test delete_user works when exists, else fails
    assert _i2b2fire.cmd.user.delete("cli_test") is None
    with pytest.raises(SystemExit):
        _i2b2fire.cmd.user.delete("cli_test")
    assert _i2b2fire.cmd.user.set_ldap("cli_test") is None
    user_list = _i2b2fire.cmd.user.list(fmt=None)
    assert {"user_id": "demo"} in user_list
    assert {"user_id": "cli_test"} not in user_list


def test_project_cli(_i2b2fire):
    _i2b2fire.conf.add_project("clitestproject")
    _i2b2fire.cmd.user.add("cli_test")
    assert _i2b2fire.cmd.group.register("clitestproject") is None
    assert _i2b2fire.cmd.group.exists("clitestproject") is None
    assert _i2b2fire.cmd.group.add_user("cli_test", "clitestproject") is None
    assert "cli_test" in _i2b2fire.cmd.group.get_members("clitestproject")
    assert _i2b2fire.cmd.group.remove_user("cli_test", "clitestproject") is None
    assert _i2b2fire.cmd.group.unregister("clitestproject") is None
    del _i2b2fire.conf["projects"]["clitestproject"]
    assert "clitestproject" not in [*_i2b2fire.cmd.group.list()]


def test_conf_add_project_in_list(_i2b2fire):
    _i2b2fire.conf.add_project("test")
    login_data = _i2b2fire.project.get_cell_login("test")
    assert "test" in list(_i2b2fire.project.list().keys())
    assert login_data == ("test_role", "test_pass")


def test_register_project(_i2b2fire):
    _i2b2fire.project.generate_sql("test")
    _i2b2fire.project.register("test", ignore_errors=True)
    assert _i2b2fire.table.get("ont_db_lookup", "i2b2demo", "test/", "@")


def test_role_can_login(_i2b2fire):
    p = _i2b2fire.conf.get_project("test")
    role, _ = _i2b2fire.project.get_cell_login("test", "crc")
    assert type(p) == GetData
    db_string = p.get("crc.db_string")
    o, e, r = psql(db_string, f"set role {role};", file=False)
    assert o == "SET\n"


def test_user_creation(_i2b2fire):
    _i2b2fire.user.add("testuser", "demouser", "Test User", force=True)
    _i2b2fire.user.add_roles("test", "testuser", roles=["TESTROLE"])
    user = _i2b2fire.user.get("testuser", fmt=None)
    roles = _i2b2fire.user.get_roles("test", "testuser")
    assert user["password"] == "9117d59a69dc49807671a51f10ab7f"
    assert roles == ["TESTROLE"]


def test_user_status_is_locked(_i2b2fire):
    _i2b2fire.table.update("user", "testuser", "status_cd", "D")
    assert _i2b2fire.status("user", "testuser") == (True, "User exists and is disabled")
    _i2b2fire.table.update("user", "testuser", "status_cd", "A")
    assert _i2b2fire.status("user", "testuser") == (True, "User exists and is active")
    with pytest.raises(SystemExit):
        _i2b2fire.cmd.user.is_locked("testuser")


def test_demo_user_registration(_i2b2fire):
    _i2b2fire.user.add("demo", "demouser2", "Demo User")
    _i2b2fire.user.add_roles("test", "demo", roles=["TESTROLE"])
    user = _i2b2fire.user.get("demo", fmt=None)
    roles = _i2b2fire.user.get_roles("test", "demo")
    # Test that user settings are not overwritten if it exits
    assert user["password"] == "9117d59a69dc49807671a51f10ab7f"
    assert roles == ["TESTROLE"]
    _i2b2fire.user.del_roles("test", "demo")
    roles = _i2b2fire.user.get_roles("test", "demo")
    assert roles == []


def test_user_ldap(_i2b2fire):
    _i2b2fire.project.set_user_ldap("test", "testuser")
    params = _i2b2fire.table.get(
        "user_param", "testuser", "search-base", fields=["value"], n="one"
    )["value"]
    _i2b2fire.project.set_user_ldap("test", "testuser", data={"search-base": "TEST"})
    params = _i2b2fire.table.get(
        "user_param", "testuser", "search-base", fields=["value"], n="one"
    )["value"]
    assert params == "TEST"


def test_user_deletion(_i2b2fire):
    _i2b2fire.user.delete("testuser")
    _i2b2fire.user.del_roles("test", "testuser")
    try:
        user = _i2b2fire.user.get("testuser")
    except Exception as err:
        user = None
    assert user == None
    shutil.rmtree(_i2b2fire.conf.sql_dir + "test_role")


def test_unregister_project(_i2b2fire):
    _i2b2fire.project.unregister("test", del_users=True)
    assert _i2b2fire.table.get("ont_db_lookup", "i2b2demo", "test/", "@", n="all") == []


def test_register_all(_i2b2fire):
    _i2b2fire.project.register_all(force=True)
    _i2b2fire.project.unregister_all()


# TESTS FOR UTILS
# ----------------
def test_fmt_data_yaml():
    data = fmt_data(["a", "b", 1, SQLException("Error")], "json")
    assert data == '["a", "b", 1, "SQLException: Error"]'
    data = fmt_data({"a": 1, 2: [1, 2, "a"]}, "yaml")
    assert data == "a: 1\n2:\n- 1\n- 2\n- a\n"


def test_pg_string():
    db_string = pg_db_string("i2b2", "demouser", "localhost", database="i2b2")
    assert db_string == TEST_CONNECTION_STRING


def test_try_exit():
    def test_function(*args, **kwargs):
        return args, kwargs

    try_exit(test_function())
    result = test_function(1, 2, 3, 4, **{"a": 1, "b": 2})
    assert result == ((1, 2, 3, 4), {"a": 1, "b": 2})


def test_encrypt_password(_i2b2fire):
    result = i2b2_password_cryptor("demouser")
    assert result == "9117d59a69dc49807671a51f10ab7f"
    result = _i2b2fire.utils.i2b2_password_cryptor("demouser")
    assert result == "9117d59a69dc49807671a51f10ab7f"
