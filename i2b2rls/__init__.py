"""
The python package `i2b2rls` provides commands to manage users
in the [i2b2](https://www.i2b2.org/) clinical data warehousing platform
and adds security enhancements with PostgreSQL Row Level Security templates.

This includes:
    * Frontend user_manager and project management
    * Datasources

## TODO
* Project as class
* POSTGRES UPDATE:
https://www.rapid7.com/db/vulnerabilities/postgres-cve-2020-25696/
"""
